ARG version

FROM docker:${version}

RUN apk update \
    && apk add \
        make \
