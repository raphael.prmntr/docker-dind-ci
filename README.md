# Docker for CI

### Purpose

Enhance docker image from https://hub.docker.com/_/docker for CI with :
* make

### Registry

https://hub.docker.com/r/alunys/docker-dind-ci

### Build

```bash
version=x.x.x make build
```
